
--[[
                                 
     Powerarrow Awesome WM theme 
     github.com/copycat-killer   
                                 
--]]

local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local spotify_widget = require("awesome-wm-widgets.spotify-widget.spotify")
local battery_widget = require("awesome-wm-widgets.battery-widget.battery")
local batteryarc_widget = require("awesome-wm-widgets.batteryarc-widget.batteryarc")
local cpu_widget = require("awesome-wm-widgets.cpu-widget.cpu-widget")
local volumearc_widget = require("awesome-wm-widgets.volumearc-widget.volumearc")
-- local pulse            = require("awesome-wm-widgets.pulseaudio-widget.pulse")

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local os, math, string = os, math, string

local theme                                     = {}
theme.taglist_spacing     = dpi(2)

-- Theme settings {{{
theme.dir                  = os.getenv("HOME") .. "/.config/awesome/themes/powerarrow-mod"
theme.wallpaper            = theme.dir .. "/WALL.jpg"
theme.border_width         = dpi(5)
theme.menu_height          = dpi(14)
theme.menu_width           = dpi(144)

theme.useless_gap          = dpi(14)

theme.tooltip_align        = "bottom"
theme.tooltip_border_width = dpi(0)
-- }}}

-- {{{ Theme fonts {{{
theme.font                     = "Fira Code 10"
theme.serif_font               = "Manuale Bold 9"
theme.mono_font                = "Fira Code 9"
theme.taglist_font             = "awesomewm-font 10"
theme.tasklist_font            = "Fira Code 11"
-- theme.icon_font                = "Webhostinghub-Glyphs 8"
theme.hotkeys_font             = "Manuale Bold 12"
theme.hotkeys_description_font = "Faustina 9"
-- }}}

-- Theme colours {{{
theme.background           = '#eceff1'
theme.foreground           = '#09212d'
theme.cursor               = '#1c222e'

theme.black1               = '#363636'
theme.black2               = '#121212'
theme.grey1                = '#bdbdbd'
theme.grey2                = '#616161'
theme.red1                 = '#e57373'
theme.red2                 = '#b71c1c'
theme.green1               = '#009688'
theme.green2               = '#00695c'
theme.yellow1              = '#fff59d'
theme.yellow2              = '#fdd835'
theme.orange1              = '#ff9900'
theme.orange2              = '#d84315'
theme.blue1                = '#b3e5fc'
theme.blue2                = '#01579b'
theme.magenta1             = '#d1c4e9'
theme.magenta2             = '#9575cd'
theme.cyan1                = '#80deea'
theme.cyan2                = '#0097a7'
theme.white1               = '#ffffff'
theme.white2               = '#dedede'

theme.fg_normal            = theme.foreground
theme.bg_normal            = theme.background
theme.fg_focus             = theme.red1
theme.bg_focus             = theme.background
theme.fg_em                = theme.grey1
theme.bg_em                = theme.grey2
theme.fg_urgent            = theme.white1
theme.bg_urgent            = theme.red1
theme.border_normal        = theme.background
theme.border_focus         = theme.green2
theme.border_marked        = theme.green1


theme.taglist_fg_empty     = theme.blue2
theme.taglist_bg_empty     = theme.white2
theme.taglist_fg_focus     = theme.white2
theme.taglist_bg_focus     = theme.blue2
theme.taglist_fg_occupied  = theme.blue2
theme.taglist_bg_occupied  = theme.white2
theme.taglist_fg_urgent    = theme.white1
theme.taglist_bg_urgent    = theme.red2


theme.tasklist_fg_focus    = theme.red2
theme.tasklist_bg_urgent   = theme.red2

theme.titlebar_fg_focus    = theme.red2
theme.titlebar_fg_normal   = theme.red1

theme.gradient_1           = theme.red1
theme.gradient_2           = theme.blue1
theme.gradient_3           = theme.blue2
theme.tooltip_border_color = theme.grey2
theme.hotkeys_bg           = theme.grey1
theme.hotkeys_modifiers_fg = theme.blue2
theme.panel_fg             = theme.grey1

theme                      = theme_assets.recolor_layout(theme, theme.panel_fg)
theme                      = theme_assets.recolor_titlebar_normal(theme, theme.titlebar_fg_normal)
theme                      = theme_assets.recolor_titlebar_focus(theme, theme.titlebar_fg_focus)

--}}}


theme.menu_submenu_icon                         = theme.dir .. "/icons/submenu.png"
theme.awesome_icon                              = theme.dir .. "/icons/awesome.png"
theme.taglist_squares_sel                       = theme.dir .. "/icons/square_sel.png"
theme.taglist_squares_unsel                     = theme.dir .. "/icons/square_unsel.png"
theme.layout_tile                               = theme.dir .. "/icons/tile.png"
theme.layout_tileleft                           = theme.dir .. "/icons/tileleft.png"
theme.layout_tilebottom                         = theme.dir .. "/icons/tilebottom.png"
theme.layout_tiletop                            = theme.dir .. "/icons/tiletop.png"
theme.layout_fairv                              = theme.dir .. "/icons/fairv.png"
theme.layout_fairh                              = theme.dir .. "/icons/fairh.png"
theme.layout_spiral                             = theme.dir .. "/icons/spiral.png"
theme.layout_dwindle                            = theme.dir .. "/icons/dwindle.png"
theme.layout_max                                = theme.dir .. "/icons/max.png"
theme.layout_fullscreen                         = theme.dir .. "/icons/fullscreen.png"
theme.layout_magnifier                          = theme.dir .. "/icons/magnifier.png"
theme.layout_floating                           = theme.dir .. "/icons/floating.png"
theme.widget_ac                                 = theme.dir .. "/icons/ac.png"
theme.widget_battery                            = theme.dir .. "/icons/battery.png"
theme.widget_battery_low                        = theme.dir .. "/icons/battery_low.png"
theme.widget_battery_empty                      = theme.dir .. "/icons/battery_empty.png"
theme.widget_mem                                = theme.dir .. "/icons/mem.png"
theme.widget_cpu                                = theme.dir .. "/icons/cpu.png"
theme.widget_temp                               = theme.dir .. "/icons/temp.png"
theme.widget_net                                = theme.dir .. "/icons/net.png"
theme.widget_hdd                                = theme.dir .. "/icons/hdd.png"
theme.widget_vol                                = theme.dir .. "/icons/vol.png"
theme.widget_vol_low                            = theme.dir .. "/icons/vol_low.png"
theme.widget_vol_no                             = theme.dir .. "/icons/vol_no.png"
theme.widget_vol_mute                           = theme.dir .. "/icons/vol_mute.png"
theme.widget_mail                               = theme.dir .. "/icons/mail.png"
theme.widget_mail_on                            = theme.dir .. "/icons/mail_on.png"
theme.widget_task                               = theme.dir .. "/icons/task.png"
theme.widget_scissors                           = theme.dir .. "/icons/scissors.png"
theme.tasklist_plain_task_name                  = false
theme.tasklist_disable_icon                     = false

theme.titlebar_close_button_focus               = theme.dir .. "/icons/titlebar/close_focus.png"
theme.titlebar_close_button_normal              = theme.dir .. "/icons/titlebar/close_normal.png"
theme.titlebar_ontop_button_focus_active        = theme.dir .. "/icons/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active       = theme.dir .. "/icons/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive      = theme.dir .. "/icons/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive     = theme.dir .. "/icons/titlebar/ontop_normal_inactive.png"
theme.titlebar_sticky_button_focus_active       = theme.dir .. "/icons/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active      = theme.dir .. "/icons/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive     = theme.dir .. "/icons/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive    = theme.dir .. "/icons/titlebar/sticky_normal_inactive.png"
theme.titlebar_floating_button_focus_active     = theme.dir .. "/icons/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active    = theme.dir .. "/icons/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive   = theme.dir .. "/icons/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive  = theme.dir .. "/icons/titlebar/floating_normal_inactive.png"
theme.titlebar_maximized_button_focus_active    = theme.dir .. "/icons/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active   = theme.dir .. "/icons/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = theme.dir .. "/icons/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = theme.dir .. "/icons/titlebar/maximized_normal_inactive.png"

local markup = lain.util.markup
local separators = lain.util.separators

-- Binary clock
local binclock = require("themes.powerarrow.binclock"){
    height = 16,
    show_seconds = true,
    color_active = theme.fg_normal,
    color_inactive = theme.bg_focus
}

-- Pulseaudio bar
-- local volume = lain.widget.pulse {
--    settings = function()
--        vlevel = volume_now.left .. "-" .. volume_now.right .. "% | " .. volume_now.device
--        if volume_now.muted == "yes" then
--            vlevel = vlevel .. " M"
--        end
--        widget:set_markup(lain.util.markup("#7493d2", vlevel))
--    end
--}

-- Calendar
theme.cal = lain.widget.calendar({
    --cal = "cal --color=always",
    attach_to = { binclock.widget },
    notification_preset = {
        font = "TerminessTTF Nerd Font Medium 8",
        fg   = theme.fg_normal,
        bg   = theme.bg_normal
    }
})

-- MEM
local memicon = wibox.widget.imagebox(theme.widget_mem)
local mem = lain.widget.mem({
    settings = function()
        widget:set_markup(markup.fontfg(theme.hotkeys_font, theme.white2, " " .. mem_now.used .. "MB "))
    end
})

-- CPU
local cpuicon = wibox.widget.imagebox(theme.widget_cpu)
local cpu = lain.widget.cpu({
    settings = function()
        widget:set_markup(markup.fontfg(theme.hotkeys_font, theme.white2, " " .. cpu_now.usage .. "% "))
    end
})

-- Coretemp (lain, average)
local temp = lain.widget.temp({
    settings = function()
       widget:set_markup(markup.fontfg(theme.hotkeys_font, theme.white2, " " .. coretemp_now .. "°C "))
    end
})
--]]
local tempicon = wibox.widget.imagebox(theme.widget_temp)

-- / fs
local fsicon = wibox.widget.imagebox(theme.widget_hdd)
theme.fs = lain.widget.fs({
    options  = "--exclude-type=tmpfs",
    notification_preset = { fg = theme.fg_normal, bg = theme.bg_normal, font = theme.font },
    settings = function()
        widget:set_markup(markup.fontfg(theme.hotkeys_font, theme.white2, " " .. fs_now.available_gb .. "GB "))
    end
})

-- Battery
local baticon = wibox.widget.imagebox(theme.widget_battery)
local bat = lain.widget.bat({
    settings = function()
        if bat_now.status ~= "N/A" then
            if bat_now.ac_status == 1 then
                widget:set_markup(markup.fontfg(theme.hotkeys_font, theme.green1, " AC "))
                baticon:set_image(theme.widget_ac)
                return
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 5 then
                baticon:set_image(theme.widget_battery_empty)
            elseif not bat_now.perc and tonumber(bat_now.perc) <= 15 then
                baticon:set_image(theme.widget_battery_low)
            else
                baticon:set_image(theme.widget_battery)
            end
        else
            widget:set_markup()
            baticon:set_image(theme.widget_ac)
        end
    end
})

-- Net
local neticon = wibox.widget.imagebox(theme.widget_net)
local net = lain.widget.net({
    settings = function()
        widget:set_markup(markup.fontfg(theme.hotkeys_font, theme.black1, " " .. net_now.received .. " ↓↑ " .. net_now.sent .. " "))
    end
})

-- Separators
local arrow = separators.arrow_left
local vert_sep = wibox.widget {
    widget = wibox.widget.separator,
    orientation = "vertical",
    forced_width = 2,
    thickness = 2,
    color = theme.border_normal,
}

function theme.powerline_rl(cr, width, height)
    local arrow_depth, offset = height/2, 0

    -- Avoid going out of the (potential) clip area
    if arrow_depth < 0 then
        width  =  width + 2*arrow_depth
        offset = -arrow_depth
    end

    cr:move_to(offset + arrow_depth         , 0        )
    cr:line_to(offset + width               , 0        )
    cr:line_to(offset + width - arrow_depth , height/2 )
    cr:line_to(offset + width               , height   )
    cr:line_to(offset + arrow_depth         , height   )
    cr:line_to(offset                       , height/2 )

    cr:close_path()
end

local function pl(widget, bgcolor, padding)
    return wibox.container.background(wibox.container.margin(widget, 16, 16), bgcolor, theme.powerline_rl)
end

function theme.at_screen_connect(s)
    -- If wallpaper is a function, call it with the screen
    local wallpaper = theme.wallpaper
    if type(wallpaper) == "function" then
        wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, true)

    -- Tags
    awful.tag(awful.util.tagnames, s, awful.layout.layouts)

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, awful.util.tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "bottom", screen = s, height = 18, bg = theme.bg_normal, fg = theme.fg_normal })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            --spr,
            s.mytaglist,
            s.mypromptbox,
            vert_sep,
            -- spr,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            s.systray,
            spotify_widget,
            -- volumearc_widget,
            -- pulse,
            -- using separators
--            arrow(theme.bg_normal, "#343434"),
--            wibox.container.background(wibox.container.margin(wibox.widget { mailicon, mail and mail.widget, layout = wibox.layout.align.horizontal }, 4, 7), "#343434"),
--            arrow("#343434", theme.bg_normal),
--            wibox.container.background(wibox.container.margin(wibox.widget { mpdicon, volume.widget, layout = wibox.layout.align.horizontal }, 3, 6), theme.bg_focus),
--            arrow(theme.bg_normal, "#343434"),
--            wibox.container.background(wibox.container.margin(task, 3, 7), "#343434"),
	    arrow(theme.white1, "#777E76"),
            wibox.container.background(wibox.container.margin(wibox.widget { memicon, mem.widget, layout = wibox.layout.align.horizontal }, 2, 3), "#777E76"),
            arrow("#777E76", "#4B696D"),
            -- wibox.container.background(wibox.container.margin(wibox.widget { cpuicon, cpu.widget, layout = wibox.layout.align.horizontal }, 3, 4), "#4B696D"),
            wibox.container.background(wibox.container.margin(wibox.widget { nil, cpu_widget, layout = wibox.layout.align.horizontal }, 3, 4), "#4B696D"),
            arrow("#4B696D", "#4B3B51"),
            wibox.container.background(wibox.container.margin(wibox.widget { tempicon, temp.widget, layout = wibox.layout.align.horizontal }, 4, 4), "#4B3B51"),
            arrow("#4B3B51", "#CB755B"),
            wibox.container.background(wibox.container.margin(wibox.widget { fsicon, theme.fs.widget, layout = wibox.layout.align.horizontal }, 3, 3), "#CB755B"),
            arrow("#CB755B", "#8DAA9A"),
            wibox.container.background(wibox.container.margin(wibox.widget { batteryarc_widget, nil, layout = wibox.layout.align.horizontal }, 3, 3), "#8DAA9A"),
            arrow("#8DAA9A", "#C0C0A2"),
            wibox.container.background(wibox.container.margin(wibox.widget { nil, neticon, net.widget, layout = wibox.layout.align.horizontal }, 3, 3), "#C0C0A2"),
            arrow("#C0C0A2", "#777E76"),
            wibox.container.background(wibox.container.margin(binclock.widget, 4, 8), "#777E76"),
            arrow("#777E76", "alpha"),
            --]]
            s.mylayoutbox,
        },
    }
end

return theme
